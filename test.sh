#!/bin/bash

IO=~colnet5/IO/
javac src/*.java -d bin/
cd bin
for i in $(seq 1 1 100)
do
	if test ! -f $IO/input.$i
	then
		echo "Tous les tests passent"
		break
	fi
	cat $IO/input.$i|java -ea Main>../trace
	output=$(diff ../trace $IO/output.$i)
	if test -n "$output"
	then
		echo "Diff etape $i"
		line=$(echo $output|head -1|cut -d c -f 1|cut -d , -f 1)
		echo "Première différence au pas" $(($line / 18))
		echo
		echo
		echo "NOUS < vs > EUX"
		diff ../trace $IO/output.$i>../diff
		head ../diff
		echo
		echo
		echo "Tout le diff dans diff"
		break
	fi
done

cd ..
