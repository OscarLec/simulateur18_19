public class EvenementArriveePassagerPalier extends Evenement {
	/* APP: Arrivee Passager Palier
       L'instant precis ou un nouveau passager arrive sur un palier donne.
	 */

	private Etage etage;

	public EvenementArriveePassagerPalier(long d, Etage edd) {
		super(d);
		etage = edd;
	}

	public void afficheDetails(StringBuilder buffer, Immeuble immeuble) {
		buffer.append("APP ");
		buffer.append(etage.numero());
	}

	public void traiter(Immeuble immeuble, Echeancier echeancier) {
		assert etage != null; 
		assert immeuble.etage(etage.numero()) == etage;
		Passager p = new Passager(date, etage, immeuble);

		if(immeuble.cabine.etage==p.etageDepart() && immeuble.cabine.porteOuverte) {
			immeuble.cabine.faireMonterPassager(p);
			echeancier.decalerFPC();
		}else {


			this.etage.ajouter(p);
			echeancier.SupprimerPAP(p);
			
		}
		echeancier.ajouter(new EvenementArriveePassagerPalier(this.date + etage.arriveeSuivante(), this.etage));
		echeancier.ajouter(new EvenementPietonArrivePalier(this.date+Global.délaiDePatienceAvantSportif, this.etage, p));

	}



}

