public class Cabine extends Global {
	/* Dans cette classe, vous pouvez ajouter/enlever/modifier/corriger les methodes, mais vous ne
       pouvez pas ajouter des attributs (variables d'instance).
	 */

	public Etage etage; // actuel, là ou se trouve la Cabine, jamais null.

	public boolean porteOuverte;

	private char intention; // 'v' ou '^'

	private Passager[] tableauPassager;
	/* Ceux qui sont actuellement dans la Cabine. On ne decale jamais les elements.
       Comme toute les collections, il ne faut pas l'exporter.
       Quand on cherche une place libre, on fait le parcours de la gauche vers la droite.
	 */

	public Cabine(Etage e) {
		assert e != null;
		etage = e;
		tableauPassager = new Passager[nombreDePlacesDansLaCabine];
		porteOuverte = false;
		intention = 'v';
	}

	public void afficheDans(StringBuilder buffer) {
		buffer.append("Contenu de la cabine: ");
		for (Passager p: tableauPassager) {
			if (p == null) {
				buffer.append("null");		
			} else {
				p.afficheDans(buffer);
			}
			buffer.append(' ');
		}
		assert (intention == 'v') || (intention == '^');
		buffer.append("\nIntention de la cabine: " + intention);
	}

	/* Pour savoir si le passager p est bien dans la Cabine.
       Attention, c'est assez lent et c'est plutôt une methode destinee à être 
       utilisee les asserts.
	 */
	public boolean transporte(Passager p) {
		assert p != null;
		for (int i = tableauPassager.length - 1 ; i >= 0  ; i --) {
			if (tableauPassager[i] == p) {
				return true;
			}
		}
		return false;
	}

	public char intention() {
		assert (intention == 'v') || (intention == '^');
		return intention;
	}

	public void changerIntention(char s){
		assert (s == 'v') || (s == '^');
		intention = s;
	}

	public void changerIntention() {
		if(this.intention=='v') {
			this.intention = '^';
		}else {
			this.intention = 'v';
		}
	}

	public boolean faireMonterPassager(Passager p) { 
		assert p != null;
		assert ! transporte(p);
		if (isModeParfait()) {
			if (intention != p.sens()) {
				return false;
			}
		}
		for (int i=0 ; i<tableauPassager.length ; i++) {

			if(tableauPassager[i]==null){
				
				tableauPassager[i]=p;
				return true;
			}
		}
		return false;
	}

	public int faireDescendrePassagers(Immeuble immeuble,long d){
		int c=0;
		if(this.passagersVeulentDescendre()) {
			int i=tableauPassager.length-1;
			while(i>=0){
				if(tableauPassager[i]!=null){
					assert transporte(tableauPassager[i]);
					if(tableauPassager[i].etageDestination() == etage){
						immeuble.ajouterCumul(d-tableauPassager[i].dateDepart());
						immeuble.nombreTotalDesPassagersSortis++;
						tableauPassager[i]=null; 
						c++;
					}
				}
				i--;
			}
		}
		return c;
	}

	public boolean passagersVeulentDescendre(){
		int i=tableauPassager.length-1;
		while(i>=0){
			if(tableauPassager[i]!=null){
				assert transporte(tableauPassager[i]);
				if(tableauPassager[i].etageDestination() == etage){
					return true;
				}
			}
			i--;
		}
		return false;
	}

	public boolean auMoinUnPlace() {
		for (int i=0 ; i<tableauPassager.length ; i++) {
			if(tableauPassager[i]==null){
				return true;
			}
		}
		return false;
	}

	public boolean estVide() {
		return this.tableauPassager[0]==null;
	}

	public Etage etageSuivant(Immeuble immeuble) {
		if(this.intention=='^') {
			return immeuble.etage(this.etage.numero()+1);
		}else {
			return immeuble.etage(this.etage.numero()-1);
		}

	}

}
