public class EvenementPietonArrivePalier extends Evenement {
    /* PAP: Pieton Arrive Palier
       L'instant precis ou un passager qui à decide de continuer à pieds arrive sur un palier donne.
       Classe à faire complètement par vos soins.
    */

	public Passager p;
    private Etage etage;
	
    public void afficheDetails(StringBuilder buffer, Immeuble immeuble) {
    	buffer.append("PAP");
    }

    public void traiter(Immeuble immeuble, Echeancier echeancier) {
	
    }

    public EvenementPietonArrivePalier(long d, Etage e, Passager p) {
	// Signature approximative et temporaire... juste pour que cela compile.
	super(d);
	this.etage = e;
	this.p = p;
    }
    
}
