public class EvenementOuverturePorteCabine extends Evenement {
    /* OPC: Ouverture Porte Cabine
       L'instant precis ou la porte termine de s'ouvrir. 
    */

    public EvenementOuverturePorteCabine(long d) {
	super(d);
    }

    public void afficheDetails(StringBuilder buffer, Immeuble immeuble) {
	buffer.append("OPC");
    }

    public void traiter(Immeuble immeuble, Echeancier echeancier) {
	Cabine cabine = immeuble.cabine;
	Etage etage = cabine.etage;
	assert ! cabine.porteOuverte;
	
	cabine.porteOuverte = true;

	
	int comp = cabine.faireDescendrePassagers(immeuble, this.date);
		
	etage.calcIntention();
	if(cabine.estVide()) {
		immeuble.chercherPersonneProche();
	}
	
	comp+=etage.faireMonterPassager(echeancier, this.date);
	
	
	
	assert cabine.porteOuverte;
	
	echeancier.ajouter(new EvenementFermeturePorteCabine(this.date+comp*this.tempsPourEntrerOuSortirDeLaCabine+this.tempsPourOuvrirOuFermerLesPortes));

    }

}

